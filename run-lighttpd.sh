#!/bin/bash

set -e

chown www-data: /dev/{stderr,stdout}
/usr/sbin/lighttpd -Df /etc/lighttpd/lighttpd.conf
